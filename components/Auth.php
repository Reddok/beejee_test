<?php

    namespace Components;

    /**
     * class Auth handles with authentication for admin
     *
     *  methods:
     *
     *  * checkUserSession
     *  * authenticate
     *
     */

    class Auth {


        /**
         * This method performs authenticate for admin
         *
         *
         */

        public static function authenticate() {
            $_SESSION['admin'] = true;
        }

        public static function isAdmin() {
            return isset($_SESSION['admin']);
        }

        /**
         * Clear variable admin
         *
         */

        public static function clear() {
            unset($_SESSION['admin']);
        }

    }