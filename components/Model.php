<?php

namespace Components;

/**
 * abstract class Model
 *
 *  methods:
 *
 *  * countRows
 *
 */

abstract class Model implements Interfaces\ModelInterface
{

    /**
     * Instance of database
     *
     * @var object
     */

    public $connect;

    /**
     * Name of table associated with this model
     *
     * @var string
     */

    protected $tableName;

    /**
     * Constructor. Receive instance of database
     *
     */

    public function __construct()
    {
        $this->connect = Database::getInstance();
    }

    /**
     * Count all entries for table of this model
     *
     * @return int
     *
     */

    public function countRows()
    {

        $q      = 'SELECT COUNT(*) as count FROM ' . $this->tableName . ';';
        $result = $this->connect->execute($q, [], false);

        return $result['count'];

    }

    /**
     * Deletes entity
     *
     * @param int           $id     The id of the entity
     *
     * @return bool
     *
     */

    public function delete($id) {

        $q = 'DELETE ' . $this->tableName . ' WHERE id=?;';
        return $this->connect->execute($q, [$id]);

    }

}