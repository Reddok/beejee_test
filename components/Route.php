<?php

namespace Components;

/**
 * class Route which acts as simple router
 *
 *  methods:
 *
 *  * start
 *  * runController
 *  * checkAccess
 *  * findPath
 *  * select
 */

class Route
{
    /**
     * array of routes
     *
     * @var object
     */

     private $_routes;

    /**
     * Constructor. Receive application routes and initialize transformations
     *
     * @param array             $routes     routes from application
     */

    function __construct(array $routes)
    {
        $this->_routes = $routes;
    }

    /**
     * starts router
     *
     * @throws \Exception       error, if path not found
     */

    function start() {

        $uri  = '';
        $path = '';


        $uri = $this->getURI();

        foreach( $this->_routes as $urlPattern => $path) {

            if( preg_match("~^$urlPattern$~", $uri) ) {

                $path = preg_replace("~$urlPattern~", $path, $uri);

                $this->runController($path);
                return;

            }

        }

        throw new \Exception(404);

    }

    /**
     * returns request uri
     *
     * @return string
     */

    private function getURI() {

        if( !empty( $_SERVER['REQUEST_URI'] ) ) {
            $uri = strtok($_SERVER["REQUEST_URI"],'?');
            return trim($uri, '/');
        }

    }

    /**
     * runs controller
     *
     * @param string             $path     string which indicates controller and action
     *
     * @throws \Exception             throws exception controller or method doesn't exist
     */

    public function runController($path) {

        $arguments  = null;
        $controller = null;
        $action     = null;

        $arguments = explode('/', $path);

        $controller = ucfirst( array_shift($arguments) . 'Controller' );
        $action = array_shift($arguments) . 'Action';

        if( class_exists($controller) ) {

            $controller = new $controller();

        } else {

            throw new \Exception(404);

        }

        if( method_exists($controller, $action) ) {

            $controller->$action(...$arguments);

        } else {

            throw new \Exception(404);

        }

    }


}