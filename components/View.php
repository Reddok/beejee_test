<?php

namespace Components;

/**
 * class View which works with views, templates, partials and other trash
 *
 *  methods:
 *
 *  * render
 *
 *
 */

class View implements Interfaces\ViewInterface
{

    /**
     * There will be globals. It is variables that will be present in each rendered view.
     * But they can be overwritten if data with the same names is transmitted
     *
     * @var array
     */

    static private $globals = [];

    /**
     * render specified view
     *
     * @param string            $template       path to layout file
     * @param string            $content_page   path to view file
     * @param array             $data           data for view
     * @param string            $title          title for view. Completely optional
     *
     */

    public function render($template, $content_page, $data = [], $title="Main")
    {

        extract(static::$globals);
        if(is_array($data)) extract($data);

        if($template) {
            require_once ROOT . '/app/templates/' . $template;
        } else {
            require_once ROOT . '/app/views/' . $content_page;
        }

    }

    /**
     * registers new global
     *
     * @param string            $key       name of global
     * @param mixed             $value     global value
     *
     */

    static public function registerGlobals($key, $value) {
        static::$globals[$key] = $value;
    }
}