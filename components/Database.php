<?php

namespace Components;

/**
 * class Database which represent database connection
 *
 *  methods:
 *
 *  * execute
 *  * getConnect
 *  * getInstance
 *
 */

class Database
{

    /**
     * Single instance of database
     *
     * @var object
     */

    private static $_instance;

    /**
     * Constructor.
     *
     * Create new database connection. Private because implement singleton pattern
     */

    private function __construct() {

        $this->connect = new \PDO('mysql:host=' . HOST . ';dbname=' . DBNAME . ';charset=utf8', USER, PASSWORD, [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]);

    }

    /**
     * This method execute all database operations
     *
     * @param string           $sql         Query which will be performed
     * @param array            $parameters  Query parameters
     * @param bool             $all         If statement is select, return all entries or only first
     * @param int              $getType     If statement is select, indicates the type in which the data will be returned
     *
     * @return mixed
     */

    public function execute($sql, $parameters=[], $all=true, $getType=\PDO::FETCH_ASSOC) {

            $stmt      = null;
            $type      = null;
            $operation = null;
            $entries   = null;


            $stmt = $this->connect->prepare($sql);
            $type = strtoupper( explode(' ', $sql)[0] );

            foreach($parameters as $pos => $value) {
                $value = $value === ''? null : $value;
                $stmt->bindValue($pos + 1, $value);
            }

            $operation = $stmt->execute();

            switch($type) {

                case 'SELECT':
                    $entries = $stmt->fetchAll($getType);

                    $entries = array_map(function($elem) {

                        return array_map(function($field) {
                            return $field === 'NULL'? '' : $field;
                        }, $elem);

                    }, $entries);

                    return $all? $entries : $entries[0];

                case 'INSERT':
                    return $this->connect->lastInsertId();

                default:
                    return $operation;

            }

    }

    /**
     * Return underlying connect
     *
     * @return object
     */

    public function getConnect() {
        return $this->connect;
    }


    private function __clone() {}

    /**
     * Return(create if not exist) instance of class Database
     *
     * @return object
     */

    public static function getInstance() {

        if(!self::$_instance) self::$_instance = new self();
        return self::$_instance;
    }


}