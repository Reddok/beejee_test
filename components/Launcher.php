<?php

namespace Components;

/**
 * main class.
 *
 *  methods:
 *
 *  * runController
 *  * start
 *  * aetDebugMode
 *  * setErrorHandler
 *  * handleError
 *
 */

class Launcher {

    /**
     * There will be instance of class Route
     *
     * @var object
     */

    private $router;


    /**
     * Custom error handler
     *
     * @var callable
     */

    private $errorHandler;

    /**
     * Variable which indicates, it's debug mode or not
     *
     * @var bool
     */

    private $debugMode = true;

    /**
     * Constructor. Creates initial utility object and set debug mode
     *
     * @param array         $routes         array of routes from application
     *
     */

    public function __construct(array $routes) {

        $this->router = new Route($routes);

        $this->errorHandler = function(\Exception $e) {
            throw $e;
        };

        if( defined('DEBUG' )) $this->setDebugMode(DEBUG);

    }


    /**
     * runs router's controller.
     *
     * @param string         $path         controller path
     *
     */

    public function runController($path) {
        $this->router->runController($path);
    }

    /**
     * Starts framework. Collects all request data and send it through all middleware to appropriate controller
     *
     */

    public function start() {

        try {
            $this->router->start();
        } catch(\Exception $e) {
            $this->handleError($e);
        }

    }

    /**
     * Sets debug mode;
     *
     * @param bool          $mode   debug mode
     *
     */

    public function setDebugMode(bool $mode) {
        $this->debugMode = $mode;
    }

    /**
     * Sets error handler;
     *
     * @param callable          $handler  custom function which will handle error
     *
     */

    public function setErrorHandler(callable  $handler) {
        $this->errorHandler = $handler;
    }

    /**
     * Catch exception an chooses appropriate handler;
     *
     * @param \Exception          $e  exception
     *
     */

    public function handleError(\Exception $e) {
        ($this->errorHandler)($e);
    }

}