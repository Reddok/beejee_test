<?php

namespace Components\Interfaces;

/**
 * Interface methods from which the View  has to implement
 *
 * abstract methods:
 *
 *  * render
 *
 */

interface ViewInterface {

    public function render($template, $contentPage);

}