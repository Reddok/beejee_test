<?php

use Components\{Launcher, Controller};

session_start();

define('ROOT', __DIR__);

require_once ROOT . '/app/config/config.php';
require_once ROOT . '/vendor/autoload.php';

$routes = require ROOT . '/app/routes.php';

$launcher = new Launcher($routes);

$launcher->setErrorHandler(function(Exception $e) {

    $code = $e->getMessage();
    Controller::redirect('error/' . $code);

});

$launcher->start();