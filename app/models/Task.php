<?php

use Components\Model;

/**
 * class Task represents task in todo list
 *
 *  methods:
 *
 *  * getAll
 *  * getById
 *  * create
 *  * update
 *  * delete
 *
 *
 */


class Task extends Model
{
    /**
     * Name of table
     *
     * @var string
     */

    protected $tableName = 'task';


    /**
     * returns a certain number of tasks in a certain order
     *
     * @param int    $limit
     * @param int    $offset
     * @param string    $orderColumn
     * @param string    $orderDirection
     *
     * @return array
     */


    public function getAll($orderColumn = 'id', $orderDirection = 'ASC', $limit = 3, $offset = 0)
    {

        $sql = 'SELECT * FROM ' . $this->tableName
            . ' ORDER BY ' .  $orderColumn . ' ' . $orderDirection
            . ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';

        $items = $this->connect->execute($sql);

        foreach($items as &$item) {
            $item['thumbnail'] = $item['thumbnail'] ?? DEFAULT_THUMBNAIL;
        }

        return $items;

    }

    /**
     * returns the task by id
     *
     * @param int    $id
     *
     *
     * @return array
     */

    public function getById($id)
    {

        $sql = 'SELECT * FROM ' . $this->tableName . ' WHERE id=' . $id;
        $item = $this->connect->execute($sql, [], false);

        if( $item ) {
            $item['thumbnail'] = $item['thumbnail'] ?? DEFAULT_THUMBNAIL;
        }

        return $item;

    }

    /**
     * creates new task
     *
     * @param array    $data
     *
     * @return int
     */

    public function create($data)
    {

        $q = 'INSERT INTO ' . $this->tableName
            . '(username, email, title, description, thumbnail)'
            . ' VALUES(?, ?, ?, ?, ?);';

        return $this->connect->execute($q, [
            $data['username'],
            $data['email'],
            $data['title'],
            $data['description'],
            $data['thumbnail']
        ]);

    }

    /**
     * creates new task
     *
     * @param int      $id
     * @param array    $data
     *
     *
     * @return bool;
     */

    public function update($id, $data)
    {

        $q = 'UPDATE ' . $this->tableName
            . ' SET title = ?, description = ?, status = ?'
            . ' WHERE id = ?';

        return $this->connect->execute($q, [
            $data['title'],
            $data['description'],
            $data['status'],
            $id
        ]);

    }

}