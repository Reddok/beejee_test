<nav aria-label="Page navigation" class="text-center">
    <ul class="pagination pagination-lg">

        <?php if(!$pagination['prev']): ?>
            <li class="disabled">
                    <span aria-hidden="true">&laquo;</span>
            </li>
            <li  class="disabled">
                    <span aria-hidden="true">&lsaquo;</span>
            </li>
        <?php else: ?>
            <li>
                <a href="<?=$pagination['path']?>page=1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li>
                <a href="<?=$pagination['path']?>page=<?=$pagination['prev']?>" aria-label="Previous">
                    <span aria-hidden="true">&lsaquo;</span>
                </a>
            </li>
        <?php endif; ?>

       <?php if($pagination['nearest'][0] > 1 ): ?>
            <li class="disabled" > <span aria-hidden="true">...</span> </li>
       <?php endif ?>

        <?php foreach($pagination['nearest'] as $value):?>

            <li <?php if($value == $pagination['current']): ?> class="active" <?php endif; ?> >
                <a href="<?=$pagination['path']?>page=<?=$value?>"><?=$value?></a>
            </li>

        <?php endforeach; ?>

        <?php if( $pagination['nearest'][ count($pagination['nearest']) - 1 ]  < $pagination['pages'] ): ?>
            <li class="disabled" > <span aria-hidden="true">...</span>  </li>
        <?php endif ?>

        <?php if(!$pagination['next']): ?>
            <li  class="disabled">
                <span aria-hidden="true">&rsaquo;</span>
            </li>
            <li >
                <span aria-hidden="true"> &raquo; </span>
            </li>
        <?php else: ?>
            <li>
                <a href="<?=$pagination['path']?>page=<?=$pagination['next']?>" aria-label="Previous">
                    <span aria-hidden="true">&rsaquo;</span>
                </a>
            </li>
            <li >
                <a href="<?=$pagination['path']?>page=<?=$pagination['pages']?>">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        <?php endif; ?>
    </ul>
</nav>