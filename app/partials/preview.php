<div class="row">
    <div class="col-md-12">
        <div class="task">
            <div class="row">
                <div class="col-md-4">
                    <figure class="thumbnail-container">
                        <img src="<?=DEFAULT_THUMBNAIL?>" alt="" data-preview="thumbnail">
                    </figure>
                </div>
                <div class="col-md-8">
                    <div class="meta-data text-right">
                        <p data-preview="username"></p>
                        <p data-preview="email"></p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="task-data">
                        <div class="task-title">
                            &mdash; <span data-preview="title"></span>
                        </div>
                        <div class="task-content" data-preview="description">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
