<?php


    return [

        'tasks'                  => 'task/index',
        'tasks/create'           => 'task/create',
        'tasks/([0-9]+)'         => 'task/show/$1',
        'tasks/([0-9]+)/edit'    => 'task/edit/$1',
        'tasks/([0-9]+)/delete'  => 'task/delete/$1',
        'login'                  => 'user/login',
        'logout'                 => 'user/logout',
        'error/([0-9]+)'         => 'error/show/$1',
        ''                       => 'task/index'

    ];