<h1>Create new task</h1>

<form action="/tasks/create" enctype="multipart/form-data" method="post" class="create-form" id="createForm">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="username">Username:</label>
                <input type="text" class="form-control" name="task[username]" id="username" placeholder="Reddok..." required>
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="task[email]" id="email" placeholder="reddok@gmail.com..." required>
            </div>
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" name="task[title]" id="title" placeholder="Buy something..." required>
            </div>
            <div class="form-group">
                <label for="description">Task description:</label>
                <textarea class="form-control" id="description" name="task[description]" placeholder="What to do?" required></textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group image-group">
                <figure class="thumbnail-container">
                    <img src="/public/images/defaultThumbnail.png" alt="thumbnail" id="uploadPreview">
                </figure>
                <label>
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
                    <input type="file" name="thumbnail" data-image="uploadPreview" accept="image/jpeg,image/png,image/gif"/>
                </label>
            </div>
        </div>
    </div>

    <a class="btn btn-primary" href="/tasks/preview" data-toggle="modal" data-target="#previewTaskContainer" data-preview="previewTask" data-preview-src="createForm">Preview</a>
    <button class="btn btn-primary" type="submit">Create task</button>
</form>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="previewTaskLabel" id="previewTaskContainer">
    <div class="modal-dialog modal-lg" role="document">
        <div class="content-modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Preview for task</h4>
            </div>
            <div class="modal-body" id="previewTask">
                <?php require ROOT . '/app/partials/preview.php' ?>
            </div>
        </div>
    </div>
</div>

