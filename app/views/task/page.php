<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="task">
            <div class="row">
                <div class="col-md-4">
                    <figure class="thumbnail-container">
                        <img src="<?=$task['thumbnail']?>" alt="">
                    </figure>
                </div>
                <div class="col-md-8">
                    <div class="meta-data text-right">
                        <p><?=$task['username']?></p>
                        <p><?=$task['email']?></p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="task-data">
                        <div class="task-title">
                            &mdash; <?=$task['title']?>
                        </div>
                        <div class="task-content">
                            <?=$task['description'] ?? 'No description...'?>
                        </div>
                    </div>

                    <?php if( \Components\Auth::isAdmin() ): ?>
                        <a href="/tasks/<?=$task['id']?>/edit" class="btn btn-primary">Edit task</a>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>
