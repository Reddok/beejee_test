<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Task list:</h1>

        <a href="/tasks/create" class="btn btn-primary task-add-button">Add new task</a>
        <table class="table table-striped table-hover text-center task-list">
            <thead>
            <tr>

                <?php foreach($labels as $label): ?>
                        <th>
                            <a href="/tasks?order=<?=$label?>&direction=<?=$_GET['order'] === $label && $_GET['direction'] === 'desc'? 'asc' : 'desc'?>">
                                <?=ucfirst($label)?>
                            </a>
                        </th>
                <?php endforeach; ?>

            </tr>
            </thead>

            <tbody>

            <?php if($tasks): ?>



                    <tr class="task-preview <?php if($task['status']): ?>completed <?php endif ?>">

                        <td><a href="/tasks/<?=$task['id']?>"><span class="glyphicon glyphicon-<?=$task['status']? 'ok' : 'minus'?>"></span></a></td>
                        <td><a href="/tasks/<?=$task['id']?>"><b><?=$task['title']?></b></a></td>
                        <td><a href="/tasks/<?=$task['id']?>"><?=$task['username']?></a></td>
                        <td><a href="/tasks/<?=$task['id']?>"><?=$task['email']?></a></td>

                    </tr>



            <?php else: ?>

                <tr class="empty-list">
                    <td colspan="4"> No work, no money! </td>
                </tr>

            <?php endif; ?>

            </tbody>
        </table>


        <?php
            if($pagination['pages'] > 1) {
                require ROOT . '/app/partials/pagination.php';
            }
        ?>
    </div>
</div>




Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus, eaque, nisi. Adipisci animi aperiam delectus dicta dolorem dolorum eligendi enim est in laborum nihil nobis nulla, quae quos totam ullam vel veniam! A est ipsa neque qui saepe sequi ullam! A asperiores autem dolorum eligendi, id iste quia rem repudiandae saepe tempore. Aliquid amet blanditiis eos ex ipsa itaque laboriosam laudantium quos totam. Enim harum laborum, quae qui quos voluptas.