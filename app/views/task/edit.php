<h1>Edit task: <?=$task['title']?></h1>

<form action="/tasks/<?=$task['id']?>/edit" enctype="multipart/form-data" method="post" id="updateForm" class="update-form">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="username">Username:</label>
                <input type="text" value="<?=$task['username']?>" class="form-control" name="task[username]" id="username" placeholder="Reddok..." disabled>
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" value="<?=$task['email']?>" class="form-control" name="task[email]" id="email" placeholder="reddok@gmail.com..." disabled>
            </div>
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" value="<?=$task['title']?>" class="form-control" name="task[title]" id="title" placeholder="Buy something..." required>
            </div>
            <div class="form-group">
                <label for="description">Task description:</label>
                <textarea class="form-control" id="description" name="task[description]" placeholder="What to do?" required><?=$task['description']?></textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group image-group">
                <figure class="thumbnail-container">
                    <img src="<?=$task['thumbnail']?>" alt="thumbnail" id="uploadPreview">
                </figure>
            </div>
            <div class="form-group">
                <label for="status">Completed:</label>
                <input type="checkbox" value="1" name="task[status]" id="status" <?php if($task['status']): ?> checked <? endif; ?> >
            </div>
        </div>
    </div>

    <button class="btn btn-primary" type="submit">Edit task</button>
    <a class="btn btn-primary" href="/tasks/<?=$task['id']?>/delete">Delete task</a>
</form>


