<div class="row">
    <div class="col-md-6 col-md-offset-3">

        <?php foreach($errors as $error): ?>

            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                <?=$error?>
            </div>

        <?php endforeach; ?>

        <form action="/login" method="post">
            <div class="form-group">
                <label for="loginField">Login:</label>
                <input type="text" class="form-control" id="loginField" name="user[login]" required>
            </div>
            <div class="form-group">
                <label for="">Password:</label>
                <input type="password" class="form-control" id="passwordField" name="user[password]" required>
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
            <button type="reset" class="btn btn-primary">Reset</button>
        </form>
    </div>
</div>
