<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$title?></title>

    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/public/css/style.css">
</head>
<body>

<div id="page">
    <header>
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand pull-left" href="/">Task manager</a>

                    <?php if(\Components\Auth::isAdmin() ):?>
                        <a href="/logout" class="navbar-brand pull-right">
                            <span class="glyphicon glyphicon-log-out"></span>
                            Logout
                        </a>
                    <?php else: ?>
                        <a href="/login" id="loginButton" class="navbar-brand pull-right">
                            <span class="glyphicon glyphicon-log-in"></span>
                            Login
                        </a>
                    <?php endif; ?>

                </div>
            </div>
        </nav>
    </header>


    <main class="container">
        <?php require_once ROOT . '/app/views/' . $content_page; ?>
    </main>

    <footer class="navbar navbar-inverse">
        <p class="text-center copyright">Copyright &copy; Anton Katanitsia <?=date('Y');?></p>
    </footer>
</div>

<!-- JS -->
<script src="/public/js/jquery-3.2.1.min.js"></script>
<script src="/public/js/bootstrap.min.js"></script>
<script src="/public/js/main.js"></script>

</body>
</html>
