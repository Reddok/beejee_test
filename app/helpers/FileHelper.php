<?php

use claviska\SimpleImage;

/**
 * class FileHelper. Provide method for uploading files
 *
 *  methods:
 *
 *  * upload
 *
 */

class FileHelper {

    /**
     *
     * Uploads file to specified directory. Resize if needed
     *
     * @param array         $file   file for upload
     *
     * @return string
     *
     */

    static public function upload($file) {

        $image = new SimpleImage();

        $filename = preg_replace('/(\.\w+)$/', '_' . microtime(true) * 10000 . '$1', $file['name']);
        $filePath = '/uploads/' . $filename;

        $image
            ->fromFile($file['tmp_name'])
            ->bestFit(IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT)
            ->toFile(ROOT . $filePath);

        return $filePath;
    }


}