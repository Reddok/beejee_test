<?php


class PaginationHelper {

    public static $entriesInOnePage = 3;
    public static $nearestSpan = 2;

    public static function getPagination($entries, $currentPage, $basePath) {

        $result = [];

        $result['current']  = $currentPage;
        $result['offset']   = ($currentPage - 1) * static::$entriesInOnePage;
        $result['pages']    = ceil($entries / static::$entriesInOnePage);
        $result['next']     = $result['current'] < $result['pages'] ? $result['current'] + 1 : null;
        $result['prev']     = $result['current'] > 1 ? $result['current'] - 1 : null;
        $result['nearest']  = range(max($result['current'] - 2, 1), min($result['current'] + 2, $result['pages']));
        $result['path']     = static::buildPath($basePath);

        return $result;
    }

    static private function buildPath($path) {

        $parsed = parse_url($path);
        $query  = $parsed['query'];

        parse_str($query, $params);
        unset($params['page']);

        $query = http_build_query($params);
        if($query) $query .= '&';

        return $parsed['path'] . '?' . $query;

    }
}