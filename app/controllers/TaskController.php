<?php

use Components\{Controller, Auth};


/**
 * class TaskController
 *
 *  methods:
 *
 *  * indexAction
 *  * showAction
 *  * editAction
 *  * createAction
 *  * deleteAction
 *
 *
 */

class TaskController extends Controller
{

    /**
     * Constructor
     *
     * initializes model
     *
     */

    public function __construct()
    {
        parent::__construct();
        $this->model = new Task();
    }

    /**
     * Gets task list limited and sorted in certain order
     *
     *
     */

    public function indexAction()
    {

        $order        = $_GET['order'] ?? 'id';
        $direction    = $_GET['direction'] ?? 'asc';
        $countOfTasks = $this->model->countRows();
        $labels       = ['status', 'title', 'username', 'email'];
        $basePath     = $_SERVER['REQUEST_URI'];
        $pagination   = null;
        $tasks        = null;
        $page         = null;

        if( isset($_GET['page']) && is_numeric($_GET['page']) ) {
            $page = (int) $_GET['page'];
        } else {
            $page = 1;
        }

        PaginationHelper::$entriesInOnePage = NUMBER_OF_TASKS;
        $pagination = PaginationHelper::getPagination($countOfTasks, $page, $basePath);

        $tasks = $this->model->getAll($order, $direction, NUMBER_OF_TASKS, $pagination['offset']);

        $this->view->render(
            'layout.php',
            'task/index.php',
            compact('tasks', 'pagination', 'labels'),
            'Tasks'
        );

    }

    /**
     * Show detailed information about task
     *
     * @param string    $id     task id
     *
     * @throws Exception    throws error in task with this id doesn't exist
     */

    public function showAction($id)
    {

        $task = $this->model->getById($id);

        if(!$task) throw new Exception(404);

        $this->view->render('layout.php', 'task/page.php', compact('task'), $task['title']);
    }

    /**
     * Creates new task
     *
     *
     */

    public function createAction()
    {
        $data = null;

        if($_SERVER['REQUEST_METHOD'] === 'POST') {

            $data = $_POST['task'];

           if( !empty($_FILES['thumbnail']['name']) ) {
                $data['thumbnail'] = FileHelper::upload($_FILES['thumbnail']);
            } else {
                $data['thumbnail'] = null;
            }


            $this->model->create($data);


            $this->redirect('/tasks');

        }

        $this->view->render('layout.php', 'task/create.php', [], 'Create task');

    }

    /**
     * Updates task
     *
     * @param int   $id     task id
     *
     * @throws Exception    throws error if user is not admin
     * @throws Exception    throws error if task with this id doesn't exist
     */

    public function editAction($id) {

        $task = $this->model->getById($id);
        $data = null;

        if( !Auth::isAdmin() ) throw new Exception(303);
        if( !$task ) throw new Exception(404);

        if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

            $data = $_POST['task'];

            $data['status'] = $data['status'] ?? 0;

            $this->model->update($id, $data);
            $this->redirect('/tasks');

        }

        $this->view->render('layout.php', 'task/edit.php', compact('task'), 'Edit: ' . $task['title']);

    }

    /**
     * Deletes task
     *
     * @param int   $id     task id
     *
     * @throws Exception    throws error if user is not admin
     * @throws Exception    throws error if task with this id doesn't exist
     */

    public function deleteAction($id) {

        if( !Auth::isAdmin() ) throw new Exception(303);

        $this->model->delete($id);
        $this->redirect('/tasks');

    }

}