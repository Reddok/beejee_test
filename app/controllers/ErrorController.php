<?php

use Components\{Controller};


/**
 * class TaskController
 *
 *  methods:
 *
 *  * showAction
 *
 *
 */

class ErrorController extends Controller
{

    /**
     * @var array       list of codes and appropriate error messages
     */

    static private $_errorMessages = [
        404 => 'Not Found',
        303 => 'Forbidden'
    ];

    /**
     * Display error with code and message
     *
     * @param int    $code     error code
     *
     */

    public function showAction($code)
    {

        if( isset(self::$_errorMessages[$code]) ) {

            $message = self::$_errorMessages[$code];

        } else {

            $code = 500;
            $message = 'Internal server error';

        }

        $this->view->render(
            'layout.php',
            'error/show.php',
            compact('code', 'message'),
            $message
        );
    }



}