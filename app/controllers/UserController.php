<?php

use Components\{Controller, Auth};


/**
 * class LoginController
 *
 *  methods:
 *
 *  * loginAction
 *  * logoutAction
 *
 *
 */

class UserController extends Controller
{


    /**
     * If data correct - login admin
     *
     *
     */

    public function loginAction()
    {

        $credentials = $_POST['user'];
        $errors = [];

        if($_SERVER['REQUEST_METHOD'] === 'POST') {


            if( strtolower( $credentials['login'] ) !== ADMIN_LOGIN ) {
                $errors[] = 'Incorrect login';
            }

            if( strtolower( $credentials['password'] ) !== ADMIN_PASSWORD ) {
                $errors[] = 'Incorrect password';
            }

            if( !$errors ) {
                Auth::authenticate();
                $this->redirect('/tasks');
            }

        }

        $this->view->render('layout.php', 'user/login.php', compact('errors'), 'Login');

    }

    /**
     * logout admin
     *
     */

    public function logoutAction()
    {

        Auth::clear();
        $this->redirect('/tasks');

    }

}