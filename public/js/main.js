'use strict';

$(window).ready(function() {

    var $rootElement = $(document);

    // creates preview for uploaded files

    (function() {

        $rootElement.on('change', '[type=file]', function() {

            var $el = $(this),
                $preview = $('#' + $el.attr('data-image'));
            createPreview($el, $preview);

        });

    })();


    // creates preview for task

    (function() {


        $rootElement.on('click', '[data-preview]', function(e) {

            var $el = $(this),
                containerName = $el.attr('data-preview'),
                srcName = $el.attr('data-preview-src'),
                $container = $('#' + containerName),
                $src = $('#' + srcName).find('input, select, textarea');

            $src.each( function(i, el) {

                var $el = $(el),
                    $preview = null,
                    regex = /\[(\w+)]/,
                    value = null,
                    name = null;


                if($el.attr('type') === 'file') {

                    name = $el.attr('name');
                    $preview = $container.find('[data-preview=' + name + ']').attr('src', e.target.result);
                    createPreview($el, $preview);

                } else {

                    name = regex.exec( $el.attr('name') );

                    if(name && (name = name[1]) ) {
                        value = $el.val();
                        $container.find('[data-preview=' + name + ']').text(value);
                    }

                }

            });

            e.preventDefault();

        })


    })();



});

// functions

function createPreview($input, $image) {

    var input = $input[0],
        reader = new FileReader();

    if(input.files && input.files[0]) {
        reader.onload = function (e) {
            $image.attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }


}